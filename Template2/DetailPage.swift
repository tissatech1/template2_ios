//
//  DetailPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/4/21.
//

import UIKit

class DetailPage: UIViewController {
    @IBOutlet weak var imagedetail: UIImageView!
    @IBOutlet weak var priceview: UIView!

    var imageStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        priceview.layer.cornerRadius = 10
        imagedetail.image = UIImage(named:imageStr)

    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }

    @IBAction func addtoCart(_ sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: "Added to cart!",         preferredStyle: UIAlertController.Style.alert)

//               alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
//                   //Cancel Action//
//               }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                           self.navigationController?.popViewController(animated: true)

                                        
        }))
        self.present(alert, animated: true, completion: nil)
     alert.view.tintColor = UIColor.black
        
        
    }
    
    
    
}
