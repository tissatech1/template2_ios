//
//  ViewController.swift
//  Template2
//
//  Created by TISSA Technology on 2/3/21.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var continueBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        continueBtn.isHidden = true
        continueBtn.layer.cornerRadius = 20
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.dismissAlert), userInfo: nil, repeats: false)
    }

    
    @objc func dismissAlert(){
        continueBtn.isHidden = false
    }
    
    
    @IBAction func continueBtnClicked(_ sender: Any) {
        
//        let Rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
//        self.navigationController?.pushViewController(Rest, animated: true)
        
                let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                self.navigationController?.pushViewController(login, animated: true)
        
    }
    
}

