//
//  restaurantCell.swift
//  Template2
//
//  Created by TISSA Technology on 2/5/21.
//

import UIKit

class restaurantCell: UITableViewCell {

    @IBOutlet weak var restoimage: UIImageView!
    @IBOutlet weak var locationtitle: UILabel!
    @IBOutlet weak var addresline: UILabel!
    @IBOutlet weak var baseview: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
