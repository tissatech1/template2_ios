//
//  CartCell.swift
//  Template2
//
//  Created by TISSA Technology on 2/8/21.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet weak var stepperView: UIView!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var crossbutton: UIButton!
    @IBOutlet weak var imageShow: UIImageView!
    @IBOutlet weak var productnameLbl: UILabel!
    @IBOutlet weak var costLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
