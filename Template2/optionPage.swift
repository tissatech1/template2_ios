//
//  optionPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/5/21.
//

import UIKit

class optionPage: UIViewController {

    @IBOutlet weak var optioncollection: UICollectionView!
    @IBOutlet weak var tabview: UIView!

    let optionarr = ["Menu","Profile","Orders List","Cart","About Us","Contact Us","Logout"]
    
    let optionarrImages = ["op1.png","op2.png","op3.png","op4.png","op5.png","op6.png","op7.png"]
    
    
    lazy var cellSizes: [CGSize] = {
        var cellSizes = [CGSize]()
        
        for _ in 0...100 {
            let random = Int(arc4random_uniform((UInt32(100))))
            
          //  cellSizes.append(CGSize(width: 140, height: 50 + random))
            cellSizes.append(CGSize(width: 140, height: 90))
        }
        
        return cellSizes
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
      //  layout.headerInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
//        layout.headerHeight = 50
//        layout.footerHeight = 20
        layout.minimumColumnSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        optioncollection.collectionViewLayout = layout
        
        let viewNib = UINib(nibName: "optionCell", bundle: nil)
        optioncollection.register(viewNib, forCellWithReuseIdentifier: "cell")
        
        tabview.layer.cornerRadius = 20

    }
    

    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
   
    @IBAction func restaurantloctionClicked(_ sender: UIButton) {
        
        let rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
        self.navigationController?.pushViewController(rest, animated: true)
        
    }
    
    @IBAction func orderlistClicked(_ sender: UIButton) {
        
        let Order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(Order, animated: true)
        
    }

}
// MARK: - UICollectionViewDataSource
extension optionPage: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return optionarr.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! optionCell
        
      
//        cell.backgroundimage.layer.masksToBounds = false
//        cell.backgroundimage.layer.cornerRadius = 8
//        cell.backgroundimage.clipsToBounds = true
//        cell.innerview.layer.cornerRadius = 8
//        cell.innerview2.layer.cornerRadius = 8
        
        cell.cellimage.image = UIImage(named: optionarrImages[indexPath.row])

        cell.optionname.text = optionarr[indexPath.row]
        
        
        cell.cellbaseview.layer.borderWidth = 1
        cell.cellbaseview.layer.borderColor = UIColor.black.cgColor
        cell.cellbaseview.layer.cornerRadius = 10
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == 0 {
            
            let home = self.storyboard?.instantiateViewController(withIdentifier: "CategoryPage") as! CategoryPage
            self.navigationController?.pushViewController(home, animated: true)
            
        }else if indexPath.item == 1 {
            let pro = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePage") as! ProfilePage
            self.navigationController?.pushViewController(pro, animated: true)
            
        } else if indexPath.item == 2 {
            let ord = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
            self.navigationController?.pushViewController(ord, animated: true)
            
        }else if indexPath.item == 3 {
            let cart = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
            self.navigationController?.pushViewController(cart, animated: true)
            
        }else if indexPath.item == 4 {
            let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsPage") as! AboutUsPage
            self.navigationController?.pushViewController(about, animated: true)
            
        }else if indexPath.item == 5 {
            let cont = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsPage") as! ContactUsPage
            self.navigationController?.pushViewController(cont, animated: true)
            
        }else if indexPath.item == 6 {
            
            let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
            self.navigationController?.pushViewController(login, animated: true)
            
        }
        
        
       
    }
    
}
// MARK: - CollectionViewWaterfallLayoutDelegate
extension optionPage: CollectionViewWaterfallLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return cellSizes[indexPath.item]
    }
}
