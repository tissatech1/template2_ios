//
//  BillingPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/8/21.
//

import UIKit

class BillingPage: UIViewController {
    
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var notefullView: UIView!
    @IBOutlet weak var exixtingaddBtn: UIButton!
    @IBOutlet weak var existingTitleLbl: UILabel!
    @IBOutlet weak var exixtingAddLbl: UILabel!
    @IBOutlet weak var notehalfView: UIView!
    @IBOutlet weak var fullnameLbl: UILabel!
    @IBOutlet weak var fullnameView: UIView!
    @IBOutlet weak var companynameLbl: UILabel!
    @IBOutlet weak var companyLblView: UIView!
    @IBOutlet weak var addLineLbl: UILabel!
    @IBOutlet weak var addLineView: UIView!
    @IBOutlet weak var housenumLbl: UILabel!
    @IBOutlet weak var housenumView: UIView!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var zipLbl: UILabel!
    @IBOutlet weak var zipView: UIView!
    @IBOutlet weak var countrylbl: UILabel!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var pagescrollView: UIScrollView!
    @IBOutlet weak var fullnameTopCont: NSLayoutConstraint!
    @IBOutlet weak var fullnameTf: UITextField!
    @IBOutlet weak var companynameTf: UITextField!
    @IBOutlet weak var addresslineTf: UITextField!
    @IBOutlet weak var housenumberTf: UITextField!
    @IBOutlet weak var cityTf: UITextField!
    @IBOutlet weak var zipTf: UITextField!
    @IBOutlet weak var countryTf: UITextField!
    @IBOutlet weak var stateTf: UITextField!
    @IBOutlet weak var billingNextBtn: UIButton!
    
    @IBOutlet weak var tabview: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        tabview.layer.cornerRadius = 20
        
    }
    
 
    @IBAction func nextBtnClicked(_ sender: UIButton) {
        
        let payment = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPage") as! PaymentPage
        self.navigationController?.pushViewController(payment, animated: true)
        
    }
    
    
    @IBAction func optionBtnClicked(_ sender: UIButton) {
        
        let option = self.storyboard?.instantiateViewController(withIdentifier: "optionPage") as! optionPage
        self.navigationController?.pushViewController(option, animated: true)
        
    }
    
    
    @IBAction func restaurantloctionClicked(_ sender: UIButton) {
        
        let rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
        self.navigationController?.pushViewController(rest, animated: true)
        
    }
    
    @IBAction func orderlistClicked(_ sender: UIButton) {
        
        let Order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(Order, animated: true)
        
    }

}
