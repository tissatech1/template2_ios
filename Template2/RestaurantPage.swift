//
//  RestaurantPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/5/21.
//

import UIKit

class RestaurantPage: UIViewController,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var restoTable: UITableView!
    @IBOutlet weak var tabview: UIView!

    let restoiimagearr = ["resto1.png","resto2.jpg","resto3.jpg","resto4.jpg","resto5.jpg","resto3.jpg","resto4.jpg","resto5.jpg","resto1.png","resto2.jpg"]
    
    let placearr = ["HOUSTON, TX","HOUSTON, TX","HUMBLE, TX","KATY, TX","SAN ANTONIO, TX","HOUSTON, TX","HOUSTON, TX","HUMBLE, TX","KATY, TX","SAN ANTONIO, TX"]
  //  let placenamearr = ["Durham","Memorial","Atascocita","Katy","San Antonio"]
    let placeaddarr = ["1609 Durham Houston, TX 77007","14092 Memorial Drive Houston, TX 77079","7025 FM 1960 East Humble, TX 77346","23501 Cinco Ranch Blvd.Katy, TX 77494","5238 DeZavala Road, Suite 114 San Antonio, TX 78249","1609 Durham Houston, TX 77007","14092 Memorial Drive Houston, TX 77079","7025 FM 1960 East Humble, TX 77346","23501 Cinco Ranch Blvd.Katy, TX 77494","5238 DeZavala Road, Suite 114 San Antonio, TX 78249"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        restoTable.register(UINib(nibName: "restaurantCell", bundle: nil), forCellReuseIdentifier: "cell")
        tabview.layer.cornerRadius = 20
    }
    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return restoiimagearr.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! restaurantCell

        cell.selectionStyle = .none
        

        cell.restoimage.image = UIImage(named: restoiimagearr[indexPath.row])
        cell.restoimage.layer.masksToBounds = false
        cell.restoimage.layer.cornerRadius = 8
        cell.restoimage.clipsToBounds = true
        cell.restoimage.layer.borderWidth = 1
        cell.restoimage.layer.borderColor = UIColor.black.cgColor
        
        cell.locationtitle.text = placearr[indexPath.row]
        
        cell.addresline.text = placeaddarr[indexPath.row]
        
        cell.baseview.layer.borderWidth = 0.5
        cell.baseview.layer.borderColor = UIColor.lightGray.cgColor
        cell.baseview.layer.cornerRadius = 10

        
        
        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return 118
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let Category = self.storyboard?.instantiateViewController(withIdentifier: "CategoryPage") as! CategoryPage
        self.navigationController?.pushViewController(Category, animated: true)
        
    }
    
    @IBAction func optionBtnClicked(_ sender: UIButton) {
        
        let option = self.storyboard?.instantiateViewController(withIdentifier: "optionPage") as! optionPage
        self.navigationController?.pushViewController(option, animated: true)
        
    }
    
    @IBAction func ordelistClicked(_ sender: UIButton) {
        
        let Order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(Order, animated: true)
        
    }
    
    

}
