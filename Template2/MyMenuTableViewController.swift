//
//  MyMenuTableViewController.swift
//  Template2
//
//  Created by TISSA Technology on 2/4/21.
//

import UIKit

class MyMenuTableViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var sidetableview: UITableView!
    
    let lablename = ["Profile","About Us","Contact Us","Logout"]
  //  let imagesarr = ["man.png","info.png","email.png","logout.png"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sidetableview.register(UINib(nibName: "menuCell", bundle: nil), forCellReuseIdentifier: "cell")
        
    }
    

    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return lablename.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! menuCell

        cell.selectionStyle = .none
        
//        cell.cellouter.layer.cornerRadius = 8
//        cell.cellouter.layer.shadowColor = UIColor.lightGray.cgColor
//        cell.cellouter.layer.shadowOpacity = 1
//        cell.cellouter.layer.shadowOffset = .zero
//        cell.cellouter.layer.shadowRadius = 3
        

        
        cell.celllable.text = lablename[indexPath.row]
     //   cell.cellimage.image = UIImage(named: imagesarr[indexPath.row])
        

        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        
            return 54
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
    }

    
    
}
