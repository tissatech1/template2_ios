//
//  LoginPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/13/21.
//

import UIKit

class LoginPage: UIViewController {
    @IBOutlet weak var userTF: UITextField!
    @IBOutlet weak var guestBtn: UIButton!
    @IBOutlet weak var passwordTF: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guestBtn.layer.cornerRadius = 20
        guestBtn.layer.borderWidth = 2
        guestBtn.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    @IBAction func signupClicked(_ sender: Any) {
        
        let sign = self.storyboard?.instantiateViewController(withIdentifier: "SignupPage") as! SignupPage
        self.navigationController?.pushViewController(sign, animated: true)
        
    }
    
    @IBAction func forgotusernameClicked(_ sender: Any) {
        
      //  var answer = String()
        
        let ac = UIAlertController(title: "Username  Reset", message: nil, preferredStyle: .alert)
       
        ac.addTextField { (textField) in
            textField.placeholder = "Enter your email"
            textField.textAlignment = .center
        }
        
        let submitAction = UIAlertAction(title: "OK", style: .default) { [self, unowned ac] _ in
                 let textstr = ac.textFields![0]
             //   answer = textstr.text!
                
//                if answer == ""{
//
//                    self.showSimpleAlert(messagess: "Enter email")
//                }else{
//
//                    forgotusername = answer
//
//                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
//                    usenamereset()
//
//                }
//
           }
        
        let CANCELAction = UIAlertAction(title: "CANCEL", style: .default) {  _ in
            
        }

            ac.addAction(CANCELAction)
            ac.addAction(submitAction)

            present(ac, animated: true)
        ac.view.tintColor = UIColor.black
        
        
    }
    
    @IBAction func forgotpasswordClicked(_ sender: Any) {
        
        
     //   var answer = String()
 
        let ac = UIAlertController(title: "Password  Reset", message: nil, preferredStyle: .alert)
       
        ac.addTextField { (textField) in
            textField.placeholder = "Enter your username"
            textField.textAlignment = .center
        }
        
            let submitAction = UIAlertAction(title: "OK", style: .default) { [unowned ac] _ in
                let textstr = ac.textFields![0]
           //    answer = textstr.text!
               
//               if answer == ""{
//
//                   self.showSimpleAlert(messagess: "Enter username")
//               }else{
//
//                self.forgotpassword = answer
//                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
//                self.forgotpassApi()
//
//               }
                
                
            }
        
        let CANCELAction = UIAlertAction(title: "CANCEL", style: .default) {  _ in
            
        }

            ac.addAction(CANCELAction)
            ac.addAction(submitAction)

            present(ac, animated: true)
        ac.view.tintColor = UIColor.black
        
        
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        
                let Rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
                self.navigationController?.pushViewController(Rest, animated: true)
        
    }
    
    
    @IBAction func guestClicked(_ sender: Any) {
        
        let Rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
        self.navigationController?.pushViewController(Rest, animated: true)
        
    }
    
    
    
}
