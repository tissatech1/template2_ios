//
//  SignupPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/13/21.
//

import UIKit

class SignupPage: UIViewController {

    @IBOutlet weak var blurview: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        blurview.isHidden = true
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func signupsubmitClicked(_ sender: UIButton) {
        
        
        let alert = UIAlertController(title:"Registration has been done successfully" , message: "We sent you an email for account verification",         preferredStyle: UIAlertController.Style.alert)

//               alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
//                   //Cancel Action//
//               }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                           self.navigationController?.popViewController(animated: true)

                                        
        }))
        self.present(alert, animated: true, completion: nil)
     alert.view.tintColor = UIColor.black
        
        
    }
    

}
