//
//  HomePage.swift
//  Template2
//
//  Created by TISSA Technology on 2/4/21.
//

import UIKit


class HomePage: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var cartcountView: UIView!
    @IBOutlet weak var menutable: UITableView!
    @IBOutlet weak var tabview: UIView!

    
    let imagesarr = ["dish1.jpg","dish2.jpg","dish3.jpg","dish4.jpeg","dish5.jpeg"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menutable.register(UINib(nibName: "menuCell", bundle: nil), forCellReuseIdentifier: "cell")
        tabview.layer.cornerRadius = 20
        cartcountView.layer.cornerRadius = 8
        cartcountView.layer.borderWidth = 1
        cartcountView.layer.borderColor = UIColor.systemYellow.cgColor
        
    }
    
   
    
    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! menuCell

        cell.selectionStyle = .none
        

        cell.dishimage.image = UIImage(named: imagesarr[indexPath.row])
        cell.priceview.layer.cornerRadius = 10

        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return 184
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detail = self.storyboard?.instantiateViewController(withIdentifier: "DetailPage") as! DetailPage
        
        detail.imageStr = imagesarr[indexPath.row]
        
        self.navigationController?.pushViewController(detail, animated: true)
        
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func optionBtnClicked(_ sender: UIButton) {
        
        let option = self.storyboard?.instantiateViewController(withIdentifier: "optionPage") as! optionPage
        self.navigationController?.pushViewController(option, animated: true)
        
    }
    
    @IBAction func cartBtnClicked(_ sender: UIButton) {
        
        let cart = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(cart, animated: true)
        
    }
    
    @IBAction func restaurantloctionClicked(_ sender: UIButton) {
        
        let rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
        self.navigationController?.pushViewController(rest, animated: true)
        
    }
    
    @IBAction func orderlistClicked(_ sender: UIButton) {
        
        let Order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(Order, animated: true)
        
    }
}
