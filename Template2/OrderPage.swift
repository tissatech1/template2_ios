//
//  OrderPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/9/21.
//

import UIKit

class OrderPage: UIViewController,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var orderlistTable: UITableView!
    @IBOutlet weak var tabview: UIView!
    @IBOutlet weak var orderOnlineBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        orderlistTable.register(UINib(nibName: "orderCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        tabview.layer.cornerRadius = 20
        orderOnlineBtn.layer.cornerRadius = 10
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func orderOnlineClicked(_ sender: UIButton) {
        
        let Billing = self.storyboard?.instantiateViewController(withIdentifier: "CategoryPage") as! CategoryPage

        self.navigationController?.pushViewController(Billing, animated: true)
        
    }
    
    
    @IBAction func optionBtnClicked(_ sender: UIButton) {
        
        let option = self.storyboard?.instantiateViewController(withIdentifier: "optionPage") as! optionPage
        self.navigationController?.pushViewController(option, animated: true)
        
    }
    
    
    @IBAction func restaurantloctionClicked(_ sender: UIButton) {
        
        let rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
        self.navigationController?.pushViewController(rest, animated: true)
        
    }
    
    @IBAction func orderlistClicked(_ sender: UIButton) {
        
        
        
    }

    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! orderCell

        cell.selectionStyle = .none
        
        cell.crossbutton.tag = indexPath.row
        cell.crossbutton.addTarget(self, action: #selector(DeleteBtnPredded(_:)), for: .touchUpInside)
        
        return cell
    }
    
    
    @IBAction func DeleteBtnPredded(_ sender: UIButton) {

     
        let alert = UIAlertController(title: nil, message: "Are you sure you want to cancel this order?",         preferredStyle: UIAlertController.Style.alert)

               alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
                   //Cancel Action//
               }))
        alert.addAction(UIAlertAction(title: "YES",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                           
                                        
        }))
        self.present(alert, animated: true, completion: nil)
     alert.view.tintColor = UIColor.black
        
    
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return 112
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let det = self.storyboard?.instantiateViewController(withIdentifier: "orderDetail") as! orderDetail

       
        self.navigationController?.pushViewController(det, animated: true)
        
    }
    
    
    
}
