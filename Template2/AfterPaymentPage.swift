//
//  AfterPaymentPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/10/21.
//

import UIKit

class AfterPaymentPage: UIViewController,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableheight: NSLayoutConstraint!
    @IBOutlet weak var producttable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        producttable.register(UINib(nibName: "orderDetailCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableheight.constant = 60 * 5
        
    }
    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! orderDetailCell

        cell.selectionStyle = .none
        
       
        
        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return 60
        
    }

    @IBAction func nextBtnClicked(_ sender: UIButton) {
        
        let cat = self.storyboard?.instantiateViewController(withIdentifier: "CategoryPage") as! CategoryPage
        self.navigationController?.pushViewController(cat, animated: true)
        
    }

}
