//
//  PaymentPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/10/21.
//

import UIKit

class PaymentPage: UIViewController {
    @IBOutlet weak var cardouter: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var proceedbtn: UIButton!
    @IBOutlet weak var tabview: UIView!
    @IBOutlet weak var cardwhiteview: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        tabview.layer.cornerRadius = 20
        proceedbtn.layer.cornerRadius = 10
        proceedbtn.layer.borderWidth = 2
        proceedbtn.layer.borderColor = UIColor.black.cgColor
        view1.layer.cornerRadius = 10
        view2.layer.cornerRadius = 10
        view3.layer.cornerRadius = 10
        cardouter.layer.cornerRadius = 10
        cardwhiteview.layer.cornerRadius = 10

    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func proceedBtnClicked(_ sender: UIButton) {
        
      
        
        
        let alert = UIAlertController(title: nil, message: "Payment Successful!",         preferredStyle: UIAlertController.Style.alert)

//               alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
//                   //Cancel Action//
//               }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        let summary = self.storyboard?.instantiateViewController(withIdentifier: "AfterPaymentPage") as! AfterPaymentPage

                                        self.navigationController?.pushViewController(summary, animated: true)
                                        
        }))
        self.present(alert, animated: true, completion: nil)
     alert.view.tintColor = UIColor.black
        
        
    }
   
    @IBAction func optionBtnClicked(_ sender: UIButton) {
        
        let option = self.storyboard?.instantiateViewController(withIdentifier: "optionPage") as! optionPage
        self.navigationController?.pushViewController(option, animated: true)
        
    }
    
    
    @IBAction func restaurantloctionClicked(_ sender: UIButton) {
        
        let rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
        self.navigationController?.pushViewController(rest, animated: true)
        
    }
    
    @IBAction func orderlistClicked(_ sender: UIButton) {
        
        let Order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(Order, animated: true)
        
    }
    
}
