//
//  CategoryPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/5/21.
//

import UIKit

class CategoryPage: UIViewController {
    
    @IBOutlet weak var cartcountView: UIView!
    @IBOutlet weak var categorycollection: UICollectionView!
    @IBOutlet weak var tabview: UIView!

    
    let categorymenu = ["Bergers","Juices","Appetizer","Breads","Non-veg Thali","Veg Thali","South Indian Food","Breakfast","Desserts"]
    
    let categorymenuimages = ["menu1.jpeg","menu2.jpg","menu3.jpeg","menu4.jpg","menu5.jpg","menu6.jpg","menu7.jpg","menu8.jpeg","menu9.jpeg"]
    
    lazy var cellSizes: [CGSize] = {
        var cellSizes = [CGSize]()
        
        for _ in 0...100 {
            let random = Int(arc4random_uniform((UInt32(100))))
            
          //  cellSizes.append(CGSize(width: 140, height: 50 + random))
            cellSizes.append(CGSize(width: 140, height: 140))
        }
        
        return cellSizes
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
      //  layout.headerInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
//        layout.headerHeight = 50
//        layout.footerHeight = 20
        layout.minimumColumnSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        categorycollection.collectionViewLayout = layout
        
        let viewNib = UINib(nibName: "CategoryCell", bundle: nil)
        categorycollection.register(viewNib, forCellWithReuseIdentifier: "cell")
        
        tabview.layer.cornerRadius = 20
        cartcountView.layer.cornerRadius = 8
        cartcountView.layer.borderWidth = 1
        cartcountView.layer.borderColor = UIColor.systemYellow.cgColor
        
    }
    
   
    

    
    @IBAction func optionBtnClicked(_ sender: UIButton) {
        
        let option = self.storyboard?.instantiateViewController(withIdentifier: "optionPage") as! optionPage
        self.navigationController?.pushViewController(option, animated: true)
        
    }
    
    @IBAction func orderlistClicked(_ sender: UIButton) {
        
        let Order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(Order, animated: true)
        
    }
    
    @IBAction func restaurantloctionClicked(_ sender: UIButton) {
        
        let rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
        self.navigationController?.pushViewController(rest, animated: true)
        
    }

    @IBAction func cartBtnClicked(_ sender: UIButton) {
        
        let cart = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(cart, animated: true)
        
    }
    
}

// MARK: - UICollectionViewDataSource
extension CategoryPage: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return categorymenuimages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCell
        
      
        cell.backgroundimage.layer.masksToBounds = false
        cell.backgroundimage.layer.cornerRadius = 8
        cell.backgroundimage.clipsToBounds = true
        cell.innerview.layer.cornerRadius = 8
        cell.innerview2.layer.cornerRadius = 8
        
        cell.backgroundimage.image = UIImage(named: categorymenuimages[indexPath.row])
        
        cell.itemsLable.text = categorymenu[indexPath.row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let reusableView: UICollectionReusableView?
//
//        switch kind {
//        case CollectionViewWaterfallElementKindSectionHeader:
//            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)
//            header.backgroundColor = .red
//            reusableView = header
//        case CollectionViewWaterfallElementKindSectionFooter:
//            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath)
//            footer.backgroundColor = .blue
//            reusableView = footer
//        default:
//            reusableView = nil
//        }
//
//        return reusableView!
//    }
    
    
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
}



// MARK: - CollectionViewWaterfallLayoutDelegate
extension CategoryPage: CollectionViewWaterfallLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return cellSizes[indexPath.item]
    }
}
