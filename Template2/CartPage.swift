//
//  CartPage.swift
//  Template2
//
//  Created by TISSA Technology on 2/8/21.
//

import UIKit

class CartPage: UIViewController,UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var carttable: UITableView!
    @IBOutlet weak var tabview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        carttable.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        tabview.layer.cornerRadius = 20
    }
    
    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CartCell

        cell.selectionStyle = .none
        
        cell.stepperView.layer.cornerRadius = 10
        cell.stepperView.layer.borderWidth = 1
        cell.stepperView.layer.borderColor = UIColor.darkGray.cgColor
        
        cell.outerView.layer.cornerRadius = 10
        cell.crossbutton.layer.cornerRadius = 8
        cell.outerView.layer.shadowColor = UIColor.lightGray.cgColor
        cell.outerView.layer.shadowOpacity = 1
        cell.outerView.layer.shadowOffset = .zero
        cell.outerView.layer.shadowRadius = 3
        
        cell.crossbutton.tag = indexPath.row
        cell.crossbutton.addTarget(self, action: #selector(DeleteBtnPredded(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @IBAction func DeleteBtnPredded(_ sender: UIButton) {

     
        let alert = UIAlertController(title: nil, message: "Are you sure you want to remove this item?",         preferredStyle: UIAlertController.Style.alert)

               alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
                   //Cancel Action//
               }))
        alert.addAction(UIAlertAction(title: "YES",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                           
                                        
        }))
        self.present(alert, animated: true, completion: nil)
     alert.view.tintColor = UIColor.black
        
    
    }
    
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
            return 105
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    
    

    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func checkoutBtnClicked(_ sender: UIButton) {
        
        let Billing = self.storyboard?.instantiateViewController(withIdentifier: "BillingPage") as! BillingPage

       
        self.navigationController?.pushViewController(Billing, animated: true)
        
    }
    
    @IBAction func optionBtnClicked(_ sender: UIButton) {
        
        let option = self.storyboard?.instantiateViewController(withIdentifier: "optionPage") as! optionPage
        self.navigationController?.pushViewController(option, animated: true)
        
    }
    
    
    @IBAction func restaurantloctionClicked(_ sender: UIButton) {
        
        let rest = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantPage") as! RestaurantPage
        self.navigationController?.pushViewController(rest, animated: true)
        
    }
    
    @IBAction func orderlistClicked(_ sender: UIButton) {
        
        let Order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(Order, animated: true)
        
    }

}
